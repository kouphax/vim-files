let g:gruvbox_italic=0

" load pathogen
execute pathogen#infect()

" defaults and vim-sensible overrides
set hidden                     " ensure buffers are hidden and not closed
set number                     " show line numbers
set expandtab                  " convert tabs to spaces
set nowrap                     " turn off word wrap by default
set tabstop=2                  " each <TAB> represents 2 columns
set shiftwidth=2               " text that is SHIFT indented will be indented 2 columns
set noai                       " turn off autoindent on paste
set noswapfile                 " stop swapfile being written
set wildmode=longest,list,full " set the wild mode completion
set splitright                 " vert splits to the right
set splitbelow                 " horiz splits to the bottom
set guioptions-=L              " hide left scrollbar
set guioptions-=r              " hide right scrollbar
set cursorline                 " ensure the current line is highlighted
"set guifont=Inconsolata-g:h14  " set the font to be nice and stuff
set guifont=Consolas:h14  " set the font to be nice and stuff
"set showtabline=2              " always show the tab bar

set wildignore+=*/vendor/**
set wildignore+=*/public/forum/**
set wildignore+=*/target/**
set wildignore+=node_modules/**

" auto syntax highlighting
syntax on
filetype plugin indent on

" set the colour scheme
" colorscheme gruvbox
colorscheme grb256
set bg=dark

if has("gui_macvim")
  " go full screen
  set fu
endif

" set leader
let mapleader=","

" set leader keys
nnoremap <leader>. :CtrlP<CR>
nnoremap <leader>E :%Eval<CR>
nnoremap <leader>e :Eval<CR>
map <leader>c cqp
nnoremap cpT :w<CR>:Require<CR>:Eval (clojure.test/run-all-tests)<CR>
nnoremap cpt :w<CR>:Require<CR>:Eval (clojure.test/run-tests)<CR>
nnoremap <leader>x :w<CR>:!./%<CR>
nnoremap <leader>t :w<cr>:Require<cr>:Eval (clojure.test/run-tests)<CR>
map <leader>n :NERDTreeToggle<CR>

" configure code folding
set foldmethod=syntax
set foldlevel=1
set foldminlines=1

" configure xml code folding
let g:xml_syntax_folding=1
autocmd FileType xml setlocal foldmethod=syntax
autocmd BufNewFile,BufRead *.json set ft=javascript


"enable spell checking
autocmd FileType markdown setlocal spell spelllang=en_gb

" highlight whitespace at the end of lines
highlight PoxyTabs ctermbg=cyan guibg=cyan
autocmd Syntax * syn match PoxyTabs /\t/ containedin=ALL
autocmd ColorScheme * highlight PoxyTabs ctermbg=cyan guibg=cyan
autocmd FileType diff,help,man syntax clear PoxyTabs

highlight PoxySpaces ctermbg=red guibg=red
autocmd Syntax * syn match PoxySpaces /\s\+$/ containedin=ALL
autocmd ColorScheme * highlight PoxySpaces ctermbg=red guibg=red
autocmd FileType diff,help,man syntax clear PoxySpaces

" strip trailing whitespace on save
autocmd BufWritePre * :%s/\s\+$//e

" autocreate directories on save if they don't already exist
autocmd BufWritePre * if expand("<afile>")!~#'^\w\+:/' && !isdirectory(expand("%:h")) | execute "silent! !mkdir -p ".shellescape(expand('%:h'), 1) | redraw! | endif

" make sure emmet is used only in html
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

autocmd BufRead,BufNewFile *.go set noexpandtab
autocmd BufRead,BufNewFile *.go syntax clear PoxySpaces

set rtp+=$GOPATH/src/github.com/golang/lint/misc/vim

set wildignore+=*/bgg-export/workspace/*
